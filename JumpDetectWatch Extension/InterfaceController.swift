//
//  InterfaceController.swift
//  JumpDetectWatch Extension
//
//  Created by Jessica Maria Echterhoff on 08.03.18.
//  Copyright © 2018 Jessica Echterhoff. All rights reserved.
//

import WatchKit
import Foundation
import RealmSwift 
import CoreMotion
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var session : WCSession?
    
    @IBOutlet private weak var startTracking: WKInterfaceButton!
    @IBOutlet private weak var selectGait: WKInterfaceButton!
    @IBOutlet private weak var feedback: WKInterfaceLabel!
    @IBOutlet private weak var gaitPicker: WKInterfacePicker!
    let gaits = ["Mixed", "Walk", "Trot", "Canter", "Single Jump", "Parcours"].map({(str) -> WKPickerItem in
        let picker = WKPickerItem()
        picker.title = str
        return picker
    })
    
    var selectedPicker: String = ""
    var selectedGait: String = ""
    let motionManager = CMMotionManager()
    var started: Bool = true
    
    @IBAction func selectedItem(_ value: Int) {
        self.selectedPicker = self.gaits[value].title ?? ""
    }
    @IBAction func select() {
        self.selectedGait = self.selectedPicker
        self.feedback.setText(self.selectedGait + " selected")
    }
    
    @IBAction func start() {
        if self.started {
            self.startMotionTracking()
            self.feedback.setText("Tracking started")
            self.startTracking.setTitle("Stop")
            self.sendMessage(session: session, request: ["request" : "start", "Gait": self.selectedGait ])
            self.started = false
        } else {
            self.stopMotionTracking()
            self.feedback.setText("Tracking stopped")
            self.startTracking.setTitle("Start")
            self.sendMessage(session: session, request: ["request" : "stop"])
            self.started = true
        }
    }
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        self.gaitPicker.setItems(self.gaits)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        session = WCSession.default
        session?.delegate = self
        session?.activate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        if let path = Realm.Configuration().fileURL {
            WCSession.default.transferFile(path, metadata: nil)
            print("pathis")
            print(path)
        }
        super.didDeactivate()
    }
    
    func degrees(radians:Double) -> Double {
        return 180 / Double.pi * radians
    }
    
    func sendMessage(session: WCSession?, request: [String: String]) {
        session?.sendMessage(request,
                             replyHandler: { (response) in
                                let resp = response["response"] as? String ?? "had a problem"
                                self.feedback.setText("App tracking " + resp)
        },
                             errorHandler: { (error) in
                                print("Error sending message from watch: %@", error)
        })
        try? session?.updateApplicationContext(request)
    }
    
    func stopMotionTracking() {
        motionManager.stopDeviceMotionUpdates()
        if let path = Realm.Configuration().fileURL {
            session?.transferFile(path, metadata: nil)
        }
    }
    
    
    func startMotionTracking() {
        try? session?.updateApplicationContext(["request" : "start"])
        motionManager.deviceMotionUpdateInterval = 0.01
        if motionManager.isDeviceMotionAvailable {
            let df = DateFormatter()
            df.dateFormat = "y-MM-dd H:m:ss.SSSS"
            guard let queue = OperationQueue.current else {
                return
            }
            if motionManager.isDeviceMotionAvailable, CMAltimeter.isRelativeAltitudeAvailable() {
                motionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xMagneticNorthZVertical, to: queue, withHandler: {
                    (data, error) in
                    if (error==nil) {
                        if let mydata = data {
                            let coordinate = RealmCoordinates(xAccel: mydata.userAcceleration.x, yAccel: mydata.userAcceleration.y, zAccel: mydata.userAcceleration.z, xGyro: mydata.rotationRate.x, yGyro: mydata.rotationRate.y, zGyro: mydata.rotationRate.z,xMagnet: mydata.magneticField.field.x, yMagnet: mydata.magneticField.field.y, zMagnet: mydata.magneticField.field.z, xAttit: self.degrees(radians: mydata.attitude.pitch), yAttit: self.degrees(radians:mydata.attitude.roll), zAttit: self.degrees(radians:mydata.attitude.yaw),  date: df.string(from: Date()), label: self.selectedGait + "Watch", timeInMs: Date.timeIntervalSinceReferenceDate * 1000)
                            RealmCoordinates().saveToRealm(item: coordinate)
                        }
                    }
                })
            }
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let msg = message["request"] as? String
        self.feedback.setText(msg ?? "")
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        let msg = applicationContext["request"] as? String
        let response = applicationContext["response"] as? String
        self.feedback.setText(msg ?? response)
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState:\(activationState) error:\(String(describing: error))")
    }
    
//    func session(_ session: WCSession, didFinish fileTransfer: WCSessionFileTransfer, error: Error?) {
//        NSLog("%@", "FileTransferDidCompleteWith state:\(fileTransfer) error:\(String(describing: error))")
//        self.sendMessage(session: session, request: ["request": "didSendFile"])
//    }
}
