//
//  RealmWatchCoordinates.swift
//  JumpDetectWatch Extension
//
//  Created by Jessica Maria Echterhoff on 08.03.18.
//  Copyright © 2018 Jessica Echterhoff. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift

class RealmWatchCoordinates: Object {
    @objc dynamic var xAccel: Double = 0.0
    @objc dynamic var yAccel: Double = 0.0
    @objc dynamic var zAccel: Double = 0.0
    @objc dynamic var xGyro: Double = 0.0
    @objc dynamic var yGyro: Double = 0.0
    @objc dynamic var zGyro: Double = 0.0
    @objc dynamic var xMagnet: Double = 0.0
    @objc dynamic var yMagnet: Double = 0.0
    @objc dynamic var zMagnet: Double = 0.0
    @objc dynamic var xAttit: Double = 0.0
    @objc dynamic var yAttit: Double = 0.0
    @objc dynamic var zAttit: Double = 0.0
    @objc dynamic var date: String = ""
    @objc dynamic var label: String? = nil
    @objc dynamic var accuracy: Double = 0.0
    @objc dynamic var timeInMs: Double = 0.0
    
    
    convenience init(xAccel: Double, yAccel: Double, zAccel: Double, xGyro: Double, yGyro: Double, zGyro: Double, xMagnet: Double, yMagnet: Double, zMagnet: Double, xAttit: Double, yAttit: Double, zAttit: Double, date: String, label: String?, accuracy: Double = 0.0, timeInMs: Double) {
        self.init()
        self.xAccel = xAccel
        self.yAccel = yAccel
        self.zAccel = zAccel
        self.xGyro = xGyro
        self.yGyro = yGyro
        self.zGyro = zGyro
        self.xMagnet = xMagnet
        self.yMagnet = yMagnet
        self.zMagnet = zMagnet
        self.xAttit = xAttit
        self.yAttit = yAttit
        self.zAttit = zAttit
        self.date = date
        self.label = label
        self.accuracy = accuracy
        self.timeInMs = timeInMs
    }
    
    func saveToRealm(item: RealmCoordinates) {
        if let realm = try? Realm() {
            try? realm.write {
                realm.add(item)
            }
        }
    }
}
