//
//  TrainingStatisticsViewController.swift
//  JumpDetect
//
//  Created by Jessica Maria Echterhoff on 21.03.18.
//  Copyright © 2018 Jessica Echterhoff. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class TrainingStatisticsViewController: UIViewController {
    
    @IBOutlet private weak var jumpPrediction: UILabel!
    @IBOutlet private weak var canterPrdiction: UILabel!
    @IBOutlet private weak var stridesPrediction: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dict = self.getWalkDict()
        self.canterPrdiction.text = String(describing:calculateRideTimes(times: dict["canter"] ?? [])) + " Minutes of canter"
        self.stridesPrediction.text = String(describing:calculateRideTimes(times: dict["canter"] ?? [])) + " Minutes of canter"
        self.jumpPrediction.text = String(describing:calculateRideTimes(times: dict["jump"] ?? [])) + " Minutes of jumps"
    }
    
    func getWalkDict() -> [String: [Double]]{
        let realm = try! Realm()
        let gaits = realm.objects(Features.self).toArray()
        let walk = gaits.filter({$0.gait == 0}).map({$0.time})
        let trot = gaits.filter({$0.gait == 1}).map({ $0.time})
        let canter = gaits.filter({$0.gait == 2}).map({$0.time})
        let jump = gaits.filter({$0.gait == 3}).map({$0.time})
        return ["walk": walk, "trot": trot, "canter": canter, "jump": jump]
    }
    
    func calculateRideTimes(times: [Double]) -> String {
        let dates = times.map({Date(timeIntervalSince1970: TimeInterval($0 / 1000))})
        let isToday = dates.filter({self.isToday(date: $0)})
        let min = isToday.min() ?? Date()
        let max = isToday.max() ?? Date()
        let timeinterval = max.timeIntervalSince(min)
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        return formatter.string(from: timeinterval) ?? ""
    }
    
    func isToday(date: Date) -> Bool {
        let now = Date()
        let start = startOfDay(date: now)
        let end = endOfDay(date: now) ?? Date()
        return (date > start) && (date < end)
    }
    
    
    func startOfDay(date: Date) -> Date {
        return Calendar.current.startOfDay(for: date)

    }
    
    func endOfDay(date: Date) -> Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay(date: date))
    }
}

extension Results {
    func toArray() -> [Element] {
        return self.map{$0}
    }
}
