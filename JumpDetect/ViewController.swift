//
//  ViewController.swift
//  JumpDetect
//
//  Created by Jessica Maria Echterhoff on 19.11.17.
//  Copyright © 2017 Jessica Echterhoff. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion
import WatchConnectivity
import Realm
import RealmSwift

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate, WCSessionDelegate {
    
    @IBOutlet private weak var startTraining: UIButton!
    @IBOutlet private weak var trainingStatistics: UIButton!
    @IBOutlet private weak var background: UIImageView!
    @IBOutlet private weak var addLabelButton: UIButton!
    @IBOutlet private weak var stopButton: UIButton!
    @IBOutlet private weak var startButton: UIButton!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var feedback: UILabel!
    @IBOutlet private weak var picker: UIPickerView!
    var text: String = "Hey there, welcome to JumpDetect"
    let pickerData = ["Mixed", "Walk", "Trot", "Chanter", "Single Jump", "Parcours"]
    var selectedLabel: String = "nil"
    var locationManager: CLLocationManager = {
        let locManager = CLLocationManager()
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestAlwaysAuthorization()
        locManager.activityType = .fitness
        locManager.distanceFilter = kCLDistanceFilterNone
        locManager.allowsBackgroundLocationUpdates = true
        locManager.pausesLocationUpdatesAutomatically = false
        return locManager
    }()
    
    var session = WCSession.default
    let model = gaitclassifier()
    let buffer = Buffer()
    
    @IBAction func startTraining(_ sender: Any) {
        self.startTrackingProcess(predictionAllowed: true)
    }
    @IBAction func goToStatistics(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "TrainingStatistics", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TrainingStatistics")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func save(_ sender: Any) {
        UIView.animate(withDuration: 2, animations: {
            self.picker.isHidden = true
            self.saveButton.isHidden = true
        })
        self.selectedLabel = pickerData[self.picker.selectedRow(inComponent: 0)]
        
        self.feedback.text = "Successfully saved label."
    }
    @IBAction func start(_ sender: Any) {
        self.startTrackingProcess(predictionAllowed: false)
        try? self.session.updateApplicationContext(["request" : "start"])
        print("I just tried to connect")
    }
    @IBAction func stop(_ sender: Any) {
        motionManager.stopDeviceMotionUpdates()
        locationManager.stopUpdatingLocation()
        self.feedback.text = "Successfully stopped."
        self.selectedLabel = selectedLabel + "label"
        try? self.session.updateApplicationContext(["request" : "stop"])

    }
    @IBAction func addLabel(_ sender: Any) {
        UIView.animate(withDuration: 2, animations: {
            self.picker.isHidden = false
            self.saveButton.isHidden = false
        })
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.dataSource = self;
        self.picker.delegate = self;
        self.picker.isHidden = true
        self.saveButton.isHidden = true
        self.startButton.layer.cornerRadius = 10
        self.addLabelButton.layer.cornerRadius = 10
        self.stopButton.layer.cornerRadius = 10
        self.saveButton.layer.cornerRadius = 10
        self.startTraining.layer.cornerRadius = 10
        locationManager.startUpdatingLocation()
        self.feedback.text = self.text
        session.delegate = self
        session.activate()
        NSLog("%@", "Paired Watch: \(session.isPaired), Watch App Installed: \(session.isWatchAppInstalled)")
        var images: [UIImage] = []
        for n in 20...100 {
            if let image = UIImage(named: "horse" + String(describing: n) + ".png") {
            images.append(image)
            }
        }
        self.background.animationImages = images
        self.background.animationRepeatCount = 1
        self.background.animationDuration = 10
        self.background.startAnimating()
        self.background.image = UIImage(named: "horse20.png")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let motionManager = CMMotionManager()
    let altitudeManager = CMAltimeter()
    
    func stopMotionTracking() {
        DispatchQueue.main.async {
        self.feedback.text = "Stopped Motion Tracking"
        }
        motionManager.stopDeviceMotionUpdates()
    }
    
    func degrees(radians:Double) -> Double {
        return 180 / Double.pi * radians
    }
    
    func startTrackingProcess(predictionAllowed: Bool) {
        self.startLocationTracking()
            self.startMotionTracking(predictionAllowed: predictionAllowed)
            try? self.session.updateApplicationContext(["response": "sccssflly started"])
        
        self.session.sendMessage(["response": "sccssflly started"], replyHandler: { (response) in
            if response["response"] as? String == "started" {
                self.feedback.text? = "strdt"
            }
        }, errorHandler: { (error) in
            print("Error sending message: %@", error)
        })
        try? self.session.updateApplicationContext(["response": "sccssflly method"])
    }
    
    func startLocationTracking() {
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.delegate = self
        DispatchQueue.main.async {
            self.feedback.text = "Started Location tracking"
        }
        let df = DateFormatter()
        df.dateFormat = "y-MM-dd H:m:ss.SSSS"
    }
    
    func stopLocationTracking () {
        DispatchQueue.main.async {
            self.feedback.text = "Stopped Motion Tracking"
        }
        if locationManager.allowsBackgroundLocationUpdates {
        locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let mostRecentLocation = locations.last else {
            return
        }
        if UIApplication.shared.applicationState == .active {
            print("App is foregrounded. New location is %@", mostRecentLocation)
        } else {
            print("App is backgrounded. New location is %@", mostRecentLocation)
        }
    }
    
    func startMotionTracking(predictionAllowed: Bool) {
        print("app started tracking")
        DispatchQueue.main.async {
            self.feedback.text = "Started Motion Tracking"
        }
        motionManager.showsDeviceMovementDisplay = true
        motionManager.deviceMotionUpdateInterval = 0.01
        let df = DateFormatter()
        df.dateFormat = "y-MM-dd H:m:ss.SSSS"
        guard let queue = OperationQueue.current else {
            return
        }
        if motionManager.isDeviceMotionAvailable, CMAltimeter.isRelativeAltitudeAvailable() {
            motionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xMagneticNorthZVertical, to: queue, withHandler: {
            (data, error) in
                if (error==nil) {
                    if let mydata = data {
                                    let coordinate = RealmCoordinates(xAccel: mydata.userAcceleration.x, yAccel: mydata.userAcceleration.y, zAccel: mydata.userAcceleration.z, xGyro: mydata.rotationRate.x, yGyro: mydata.rotationRate.y, zGyro: mydata.rotationRate.z,xMagnet: mydata.magneticField.field.x, yMagnet: mydata.magneticField.field.y, zMagnet: mydata.magneticField.field.z, xAttit: self.degrees(radians: mydata.attitude.pitch), yAttit: self.degrees(radians:mydata.attitude.roll), zAttit: self.degrees(radians:mydata.attitude.yaw),  date: df.string(from: Date()), label: self.selectedLabel, timeInMs: Date.timeIntervalSinceReferenceDate * 1000)
                        
                        if !predictionAllowed {
                        RealmCoordinates().saveToRealm(item: coordinate)
                        } else {
                            self.buffer.saveToBuffer(element: coordinate)
                        }
                    }
                }
            })
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        NSLog("didReceiveMessage: %@", message)
        switch message["request"] as? String {
            case "start":
                self.startTrackingProcess(predictionAllowed: false)
                print("just got the message")
                replyHandler(["response" : "started"])
            case "stop":
                self.stopMotionTracking()
                self.stopLocationTracking()
                replyHandler(["response" : "stopped"])
            case "didSendFile":
                DispatchQueue.main.async {
                    //self.feedback.text = "should receive file"
            }
            default:
            replyHandler(["response" : "nothing happened"])
        }
        if let label = message["Gait"] as? String {
            self.selectedLabel = label
        }
    }
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        let msg = applicationContext["request"] as? String ?? ""
        self.text = msg
        print("just got the context")
        print(msg)
        self.startTrackingProcess(predictionAllowed: false)
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState:\(activationState) error:\(String(describing: error))")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        NSLog("%@", "sessionDidBecomeInactive: \(session)")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        NSLog("%@", "sessionDidDeactivate: \(session)")
    }
    
    func sessionWatchStateDidChange(_ session: WCSession) {
        NSLog("%@", "sessionWatchStateDidChange: \(session)")
    }
    
    func session(_ session: WCSession, didReceive file: WCSessionFile) {
        var config = Realm.Configuration()
        config.fileURL = file.fileURL
        Realm.Configuration.defaultConfiguration = config
        
        let filemgr = FileManager.default
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPath[0] as String
        print("docsdir")
        print(docsDir)
        print(file.fileURL.absoluteString)
        do {
            try filemgr.moveItem(atPath: file.fileURL.relativeString, toPath: docsDir)
            
        } catch let error as NSError {
            print("Error moving file: \(error.description)")

        }
    }
}

