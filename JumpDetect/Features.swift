//
//  Features.swift
//  JumpDetect
//
//  Created by Jessica Maria Echterhoff on 21.03.18.
//  Copyright © 2018 Jessica Echterhoff. All rights reserved.
//

import Foundation
import RealmSwift

class Features: Object {
    @objc dynamic var gait: Int = -1
    @objc dynamic var time: Double = 0
    
    convenience init(gait: Int, time: Double) {
        self.init()
        self.gait = gait
        self.time = time 
    }
    
    func saveToRealm(item: Features) {
        if let realm = try? Realm() {
            try? realm.write {
                realm.add(item)
            }
        }
    }
}

