//
//  Buffer.swift
//  JumpDetect
//
//  Created by Jessica Maria Echterhoff on 21.03.18.
//  Copyright © 2018 Jessica Echterhoff. All rights reserved.
//

import Foundation

class Buffer {
    var elements: [RealmCoordinates] = []
    var count = 0
    let model = gaitclassifier()
    
    func saveToBuffer(element: RealmCoordinates) {
        if count >= 50 {
            let features  = [elements.map({$0.xAccel}), elements.map({$0.yAccel}), elements.map({$0.zAccel}), elements.map({$0.xGyro}), elements.map({$0.yGyro}), elements.map({$0.zGyro}), elements.map({$0.xMagnet}), elements.map({$0.yMagnet}), elements.map({$0.zMagnet}), elements.map({$0.xAttit}), elements.map({$0.yAttit}), elements.map({$0.zAttit})]
            //let dates = elements.map({$0.date})
            
            let avgs: [Double] = features.map({ return mean(elements: $0)})
            let vars: [Double] = features.enumerated().map({ (i, elem) in
                                                    return variance(elements: elem, avg: avgs[i])})
            let stds: [Double] = features.enumerated().map({ (i, elem) in
                                                    return std(variance: vars[i])})
            let mins: [Double] = features.map({$0.min() ?? 0.0})
            let maxs: [Double] = features.map({$0.max() ?? 0.0})
            let RMSs: [Double] = features.map({RMS(elements: $0)})
            //let AUCs = features.map({AUC(elements: $0)})
            
            let input = gaitclassifierInput(xAccMean: avgs[0], yAccMean: avgs[1], zAccMean: avgs[2], xGyroMean: avgs[3], yGyroMean: avgs[4], zGyroMean: avgs[5], xMagnetMean: avgs[6], yMagnetMean: avgs[7], zMagnetMean: avgs[8], xAttMean: avgs[9], yAttMean: avgs[10], zAttMean: avgs[11], xAccStd: stds[0], yAccStd: stds[1], zAccStd: stds[2], xGyroStd: stds[3], yGyroStd: stds[4], zGyroStd: stds[5], xMagnetStd: stds[6], yMagnetStd: stds[7], zMagnetStd: stds[8], xAttStd: stds[9], yAttStd: stds[10], zAttStd: stds[11], xAccVar: vars[0], yAccVar: vars[1], zAccVar: vars[2], xGyroVar: vars[3], yGyroVar: vars[4], zGyroVar: vars[5], xMagnetVar: vars[6], yMagnetVar: vars[7], zMagnetVar: vars[8], xAttVar: vars[9], yAttVar: vars[10], zAttVar: vars[11], xAccRMS: RMSs[0], yAccRMS: RMSs[1], zAccRMS: RMSs[2], xGyroRMS: RMSs[3], yGyroRMS: RMSs[4], zGyroRMS: RMSs[5], xMagnetRMS: RMSs[6], yMagnetRMS: RMSs[7], zMagnetRMS: RMSs[8], xAttRMS: RMSs[9], yAttRMS: RMSs[10], zAttRMS: RMSs[11], xAccMin: mins[0], yAccMin: mins[1], zAccMin: mins[2], xGyroMin: mins[3], yGyroMin: mins[4], zGyroMin: mins[5], xMagnetMin: mins[6], yMagnetMin: mins[7], zMagnetMin: mins[8], xAttMin: mins[9], yAttMin: mins[10], zAttMin: mins[11], xAccMax: maxs[01], yAccMax: maxs[1], zAccMax: maxs[2], xGyroMax: maxs[3], yGyroMax: maxs[4], zGyroMax: maxs[5], xMagnetMax: maxs[6], yMagnetMax: maxs[7], zMagnetMax: maxs[8], xAttMax: maxs[9], yAttMax: maxs[10], zAttMax: maxs[11])
            if let prediction = try? model.prediction(input: input) {
                Features().saveToRealm(item: Features(gait: Int(prediction.classLabel), time: Date.timeIntervalSinceReferenceDate * 1000))
            }
            elements = []
            count = 0
        }
        elements.append(element)
        count += 1
        print("elems")
        print(elements)
    }
    func AUC(elements: [Double]) -> Double {
        return 0.0
    }
    
    func mean(elements: [Double]) -> Double{
        let denominator: Double = 50.0
        return elements.reduce(0, {$0 + $1}) / denominator
    }
    
    func std(variance: Double) -> Double {
        return sqrt(variance)
    }
    
    func variance(elements: [Double], avg: Double) -> Double {
        return elements.reduce(0,{$0 + ($1 - avg)*($1 - avg)})
    }
    
    func RMS(elements: [Double]) -> Double {
        let sqrs = elements.map({$0 * $0})
        let avg = mean(elements: sqrs)
        return sqrt(avg)
    }
}
